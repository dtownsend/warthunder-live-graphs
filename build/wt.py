from urllib.request import urlopen
import http.server
import socketserver



PORT = 8112

class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
    def do_GET(self):
        print(self.wfile)

        print(self.path)
        if self.path == '/':
            with open('../wt.html', 'r') as content_file:
                content = content_file.read()
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(content.encode('utf-8'))

        elif self.path == '/state':
            html = urlopen("http://127.0.0.1:8111/state")
            state = html.read();
            print(type(state))
            state = state.decode('utf-8')
            state = state.replace("}",",")
            state = state.encode('utf-8')
            # trim last }
            html = urlopen("http://127.0.0.1:8111/indicators")
            indicators = html.read()
            indicators = indicators.decode('utf-8')
            indicators = indicators.lstrip("{")
            indicators = indicators.encode('utf-8')

            content = state + indicators

            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(content);

        else:
            with open('..'+self.path, 'r') as content_file:
                content = content_file.read()
            self.send_response(200)
            self.send_header("Content-type", "application/javascript")
            self.end_headers()
            self.wfile.write(content.encode('utf-8'))



try:
    server = http.server.HTTPServer(('localhost', PORT), MyHandler)
    print('Started http server')
    server.serve_forever()
except KeyboardInterrupt:
    print('^C received, shutting down server')
    server.socket.close()